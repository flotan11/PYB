package com.example.murschet.android;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    RequestQueue queue;
    String pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doConnect(View view){
       queue = Volley.newRequestQueue(getApplicationContext());

        final EditText et = (EditText) findViewById(R.id.LoginAccueil);
        final EditText ett = (EditText) findViewById(R.id.MdpAccueil);

        final JsonArrayRequest req = new JsonArrayRequest("http://178.33.41.228:8080/data/userdb", new Response.Listener<JSONArray> () {

            @Override
            public void onResponse(JSONArray response) {
                try {

              for (int i = 0;i<response.length();i++){
                  JSONObject test = response.getJSONObject(i);
                  if (test.has("login")) {
                      pseudo = test.getString("login");
                      if(et.getText().toString().equals(pseudo)){

                          Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                          startActivity(intent);
                      }
                  }
              }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });


        queue.add(req);



    }


    public void doInscription(View view){
        Intent intent = new Intent(getApplicationContext(), InscriptionActivity.class);
        startActivity(intent);
    }



}
