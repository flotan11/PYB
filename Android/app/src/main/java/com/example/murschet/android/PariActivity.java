package com.example.murschet.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

public class PariActivity extends AppCompatActivity {
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pari);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pari, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goMenu(View view){
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
    }

    public void validPari(View view){
        queue = Volley.newRequestQueue(getApplicationContext());

        EditText nompari = (EditText) findViewById(R.id.nompari);
        EditText miseminimum = (EditText) findViewById(R.id.miseminimum);
        EditText nomequipe1 = (EditText) findViewById(R.id.nomequipe1);
        EditText nomequipe2 = (EditText) findViewById(R.id.nomequipe2);
        EditText Description = (EditText) findViewById(R.id.Description);
        EditText password = (EditText) findViewById(R.id.password);

        HashMap<String, String> params = new HashMap<String, String>();


        params.put("description", nompari.getText().toString());
        params.put("miseMin",miseminimum.getText().toString() );
        params.put("name", nomequipe1.getText().toString());
        params.put("sideOne", nomequipe2.getText().toString());
        params.put("sideTwo", Description.getText().toString());
        params.put("password",password.getText().toString());


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,"http://178.33.41.228:8080/data/paridb", new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Error:", String.valueOf(error));

            }
        });


        queue.add(req);
    }
}
